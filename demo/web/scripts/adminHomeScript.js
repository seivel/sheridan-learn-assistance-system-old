angular.module('sortApp',[])

.controller('mainController',function($scope){
    $scope.sortType = 'name';
    $scope.sortReverse = false;
    $scope.searchFish = '';

    $scope.user = [
        {username: 'Zhangz', name:'Zhuo Zhang',email:'zhuozhang@sheridancollege.ca',role:"Learning Assistant",disabled: 'No'},
        {username: 'JasonB', name:'Jason Butterfield',email:'jasonbutt@learningadmin.ca',role:"Administrator",disabled: 'No'},
        {username: 'richS', name:'Rich Smith',email:'richsmith@sheridancollege.ca',role:"Administrator",disabled: 'No'},
        {username: 'doyleL', name:'Doyle Loius',email:'doyleloius@learningassit.ca',role:"Learning Assistant",disabled: 'No'},
        {username: 'chUrchB', name:'Church Brandon',email:'chruchbrandon@live.ca',role:"Professor",disabled: 'Yes'},
        {username: 'vanceD', name:'Vance Duncan',email:'vanceduncan@google.ca',role:"Professor",disabled: 'No'},
        {username: 'meradoC', name:'Gillespie Turner',email:'turnergill@sheridancollege.ca',role:"Learning Assistant",disabled: 'No'},
    ];
});