function displayCourseSection(){
    var programValue = $("#programSelection").val();

    if(programValue == 1){
        $("#courseSection").css("display","block");
    }else{
        $("#courseSection").css("display","none");
        $("#restSection").css("display","none");
    }
}

function displayRestSection(){
    var programValue = $("#programSelection").val();
    var courseValue = $("input[name=customRadio]:checked").val();

    if(programValue == 1 && courseValue == 1){
        $("#restSection").css("display","block");
    }else{
        $("#restSection").css("display","none");
    }
}

function displayQuestionSheet(){
    $("#questionListSection").css("display","block");
}