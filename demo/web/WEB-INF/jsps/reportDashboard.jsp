<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Report Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<c:url value="/scripts/bootstrap.min.js" />"></script>
    <script src="<c:url value="/scripts/reportScript.js" />"></script>
    <link rel="shortcut icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
    <link rel="stylesheet" href="<c:url value="/css/reportDashboardCss.css" />">
</head>
<body>
<div class="container-fluid" style="padding:0">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="<c:url value="/"/>" style="margin-left:5%; font-size:20px!important;"><img
                src='<c:url value="/images/sheridanLogo.png"/>' alt="sheridan logo" width="30px" height="40.797px"
                style="margin-right:20px;"/>Sheridan Learning Assistance System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <c:url value="/logout" var="logoutUrl"/>
            <form id="logout" action="${logoutUrl}" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
            <button class="dropdown btn btn-outline-secondary"  style="font-size:20px !important; margin-bottom:0; margin-left:80%; background:transparent; padding:0;border:none">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white; padding:0;">
                    <img src='<c:url value="/images/profileicon.png"/>' alt="profileicon" width="60px" height="60px"/> &nbsp;Admin</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-2px, 53%, 0px);">
                    <a class="dropdown-item" href="<c:url value="/resetPassword"/>">Reset the Password</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="logout()">Logout</a>
                </div>
            </button>
        </div>
    </nav>
    <div class="btn-group btn-group-toggle" id="tabsPanel">
        <a href="<c:url value="/examCram"/>" class="btn btn-primary panel-item">
            Exam Cram
        </a>
        <a href="<c:url value="/secure"/>" class="btn btn-primary panel-item">
            Accounts Management
        </a>
        <a  href="<c:url value="/questionsManagement"/>" class="btn btn-primary panel-item">
            Questions Management
        </a>
        <a  href="<c:url value="/tagsManagement"/>" class="btn btn-primary panel-item">
            Tags Management
        </a>
        <a href="<c:url value="/reports"/>" class="btn btn-primary active panel-item" >Reports</a>
    </div>
</div>
<div id="bodyContent">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active show" data-toggle="tab" href="#surveyReport">Survey Report</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#examCramReport">Exam Cram Report</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#questionCompliantReport">Question Compliant Report</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#errorLogs">Error Logs</a>
        </li>
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane fade active show" id="surveyReport">
            <table class="table" style="width:90%;margin:auto;margin-top:2%;">
                <tr>
                    <th colspan="2" style="text-align:center"><img src="<c:url value="/images/surveyReportTitle.PNG"/>" alt="surveyReportTitle"></th>
                </tr>
                <tr>
                    <td style="padding-top:5%;text-align:center;"><img src="<c:url value="/images/surveyReportBody.PNG"/>" alt="surveyReportBody"></td>
                    <td style="padding-top:5%;text-align:center;"><img src="<c:url value="/images/surveyReportChart.PNG"/>" alt="surveyReportChart"></td>
                </tr>
            </table>
        </div>
        <div class="tab-pane fade" id="examCramReport">
            <table class="table" style="width:90%;margin:auto;margin-top:2%;">
                <tr>
                    <th colspan="2" style="text-align:center"><img src="<c:url value="/images/examCramTitle.PNG"/>" alt="examCramTitle"></th>
                </tr>
                <tr>
                    <td style="padding-top:5%;text-align:center;"><img src="<c:url value="/images/examCramreportOne.PNG"/>" alt="examCramreportOne"></td>
                    <td style="padding-top:5%;text-align:center;"><img src="<c:url value="/images/examCramChartOne.PNG"/>" alt="examCramChartOne"></td>
                </tr>
                <tr>
                    <td style="padding-top:5%;text-align:center;"><img src="<c:url value="/images/examCramreportTwo.PNG"/>" alt="examCramreportTwo"></td>
                    <td style="padding-top:5%;text-align:center;"><img src="<c:url value="/images/examCramChartTwo.PNG"/>" alt="examCramChartTwo"></td>
                </tr>
            </table>
        </div>
        <div class="tab-pane fade" id="questionCompliantReport">
            <table class="table" style="width:90%;margin:auto;margin-top:2%;">
                <tr>
                    <th colspan="2" style="text-align:center"><img src="<c:url value="/images/compliantTitle.PNG"/>" alt="compliantTitle"></th>
                </tr>
                <tr>
                    <td style="padding-top:5%;text-align:center;"><img src="<c:url value="/images/compliantReport.PNG"/>" alt="compliantReport"></td>
                    <td style="padding-top:5%;text-align:center;"><img src="<c:url value="/images/compliantChart.PNG"/>" alt="compliantChart"></td>
                </tr>
            </table>
        </div>
        <div class="tab-pane fade" id="errorLogs">
            <table class="table table-hover">
                <tr>
                        <th>Time Stamp</th>
                        <th>Error Type </th>
                        <th>Message</th>
                </tr>
                <tr>
                    <td>2018-04-27 03:14:07</td>
                    <td class="text-info" style="font-weight: bold;">INFO</td>
                    <td>org.apache.catalina.core.ContainerBase.[Catalina].[localhost].level = INFO
                        </td>
                </tr>
                <tr>
                    <td>2018-04-27 8:15:46</td>
                    <td class="text-info" style="font-weight: bold;">INFO</td>
                    <td>org.apache.catalina.core.ContainerBase.[Catalina].[localhost].handlers = \
                        2localhost.org.apache.juli.FileHandler</td>
                </tr>
                <tr>
                    <td>2018-04-27 10:11:00</td>
                    <td class="text-warning" style="font-weight: bold;">WARNING</td>
                    <td>org.apache.catalina.session.level=ALL
                        java.util.logging.ConsoleHandler.level=ALL</td>
                </tr>
                <tr>
                    <td>2018-04-27 13:11:22</td>
                    <td class="text-warning" style="font-weight: bold;">WARNING</td>
                    <td>25-Apr-2018 10:23:54.203 WARNING [RMI TCP Connection(3)-127.0.0.1] org.springframework.security.config.http.FilterInvocationSecurityMetadataSourceParser.parseInterceptUrlsForFilterInvocationRequestMap Duplicate URL defined: /adminHome. The original attribute values will be overwritten</td>
                </tr>
                <tr>
                    <td>2018-04-27 13:12:22</td>
                    <td class="text-danger" style="font-weight: bold;">ERROR</td>
                    <td>[RMI TCP Connection(3)-127.0.0.1] org.springframework.security.config.http.FilterInvocationSecurityMetadataSourceParser.parseInterceptUrlsForFilterInvocationRequestMap Duplicate URL defined: /adminHome. The original attribute values will be overwritten</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<!--Footer-->
<footer class="footer bg-light">
    <div class="container" id="footer-container">
        <span class="text-muted" style="font-size:20px;">@ 2018 Copyright: Tinkers</span>
    </div>
</footer>
</body>
</html>
