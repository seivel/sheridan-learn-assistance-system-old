<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<c:url value="/scripts/bootstrap.min.js" />"></script>
    <link rel="shortcut icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
    <link rel="stylesheet" href="<c:url value="/css/reportsCss.css"/> "/>
    <title>Forget Password</title>
</head>
<body>
<div class="container-fluid" style="padding:0">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="<c:url value="/"/>" style="margin-left:5%; font-size:20px!important;"><img
                src='<c:url value="/images/sheridanLogo.png"/>' alt="sheridan logo" width="30px" height="40.797px"
                style="margin-right:20px;"/>Sheridan Learning Assistance System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
        </div>
    </nav>
    <div id="bodyContent">
        <div class="jumbotron">
            <h3 class="text-primary" style="width:80%;margin:auto;margin-bottom:2%;"><b>Forget the Password</b></h3>
            <div class="card mb-2">
                <div class="card-body">
                    <div class="form-group" style="margin-top:2%;">
                        <label class="col-form-label" for="emailAddress">Email: (Please enter the account associated email address)</label>
                        <input type="email" class="form-control" placeholder="Default input" id="emailAddress"><button class="btn btn-primary" style="margin-top:1%;">Get Code</button>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="code">Enter Code:</label>
                        <input type="text" class="form-control" placeholder="Default input" id="code">
                    </div>

                    <button class="btn btn-primary">Next</button>&nbsp;&nbsp;<button class="btn btn-primary">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!--Footer-->
    <footer class="footer bg-light">
        <div class="container" id="footer-container">
            <span class="text-muted" style="font-size:20px;">@ 2018 Copyright: Tinkers</span>
        </div>
    </footer>
</div>
</body>
</html>
