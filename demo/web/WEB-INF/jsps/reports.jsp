<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<c:url value="/scripts/bootstrap.min.js" />"></script>
    <script src="<c:url value="/scripts/reportScript.js" />"></script>
    <link rel="shortcut icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
    <link rel="stylesheet" href="<c:url value="/css/reportsCss.css"/> "/>
    <title>Reports</title>
</head>
<body>
<div class="container-fluid" style="padding:0">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#" style="margin-left:5%; font-size:20px!important;"><img
                src='<c:url value="/images/sheridanLogo.png"/>' alt="sheridan logo" width="30px" height="40.797px"
                style="margin-right:20px;"/>Sheridan Learning Assistance System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <c:url value="/logout" var="logoutUrl"/>
            <form id="logout" action="${logoutUrl}" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
            <button class="dropdown btn btn-outline-secondary"  style="font-size:20px !important; margin-bottom:0; margin-left:80%; background:transparent; padding:0;border:none">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white; padding:0;">
                    <img src='<c:url value="/images/profileicon.png"/>' alt="profileicon" width="60px" height="60px"/> &nbsp;Admin</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-2px, 53%, 0px);">
                    <a class="dropdown-item" href="<c:url value="/resetPassword"/>">Reset the Password</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="logout()">Logout</a>
                </div>
            </button>
        </div>
    </nav>
    <div class="btn-group btn-group-toggle" id="tabsPanel">
        <a href="<c:url value="/examCram"/>" class="btn btn-primary panel-item">
            Exam Cram
        </a>
        <a href="<c:url value="/secure"/>" class="btn btn-primary panel-item">
            Accounts Management
        </a>
        <a  href="<c:url value="/questionsManagement"/>" class="btn btn-primary panel-item">
            Questions Management
        </a>
        <a  href="<c:url value="/tagsManagement"/>" class="btn btn-primary panel-item">
            Tags Management
        </a>
        <a href="<c:url value="/reports"/>" class="btn btn-primary active panel-item" >Reports</a>
    </div>
</div>

<div id="bodyContent">
    <div class="jumbotron">
        <h3 id="reportTitle" class="text-primary"><b>Exam Cram Reports</b></h3>
        <div class="card mb-2">
            <div class="card-body">
                <table class="table table-hover borderless" border="0">
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_03_09</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_03_10</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_03_11</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_03_12</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_03_13</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_03_14</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_04_09</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_04_10</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_04_11</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_04_12</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_04_13</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_04_14</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_05_16</a></td></tr>
                    <tr><td><a href="<c:url value="/reportDashboard"/>">Exam Cram Report - 2018_05_19</a></td></tr>
                </table>
            </div>
        </div>
    </div>
</div>

<!--Footer-->
<footer class="footer bg-light">
    <div class="container" id="footer-container">
        <span class="text-muted" style="font-size:20px;">@ 2018 Copyright: Tinkers</span>
    </div>
</footer>

</body>
</html>