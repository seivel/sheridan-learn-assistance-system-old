<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<c:url value="/scripts/bootstrap.min.js" />"></script>
    <script src="<c:url value="/scripts/script.js" />"></script>
    <script src="<c:url value="/scripts/studentExamCramScript.js" />"></script>
    <link rel="shortcut icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
    <link rel="stylesheet" href="<c:url value="/css/style.css"/> "/>
    <link rel="stylesheet" href="<c:url value="/css/adminHomeCss.css"/> "/>
    <title>Student Exam Cram</title>
    <style>
        .badge{
            font-size:12px;
        }
    </style>
</head>
<body>
<div class="container-fluid" style="padding:0">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="<c:url value="/"/>" style="margin-left:5%; font-size:20px!important;"><img
                src='<c:url value="/images/sheridanLogo.png"/>' alt="sheridan logo" width="30px" height="40.797px"
                style="margin-right:20px;"/>Sheridan Learning Assistance System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <c:url value="/" var="logoutUrl"/>
            <form id="logout" action="${logoutUrl}" method="get">
            </form>
            <button class="dropdown btn btn-outline-secondary"  style="font-size:20px !important; margin-bottom:0; margin-left:80%; background:transparent; padding:0;border:none">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white; padding:0;">
                    Student: Eric Y <br />ID: 991407536</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-2px, 100%, 0px);">
                    <a class="dropdown-item" href="#" onclick="logout()">Logout</a>
                </div>
            </button>
        </div>
    </nav>
</div>
<div id="bodyContent">
    <div class="jumbotron">
        <div style="width: 80%;margin: auto;">
            <div class="card mb-2">
                <div class="card-body">
                    <div id="programSection">
                        <div class="form-group">
                            <b class="text-primary">Program:</b>
                            <br />
                            <br />
                            <select class="custom-select" style="width:30%" id="programSelection" onclick="displayCourseSection()">
                                <option>Select the Program</option>
                                <option value="1">Computer Science</option>
                                <option value="2">Animation Design</option>
                                <option value="3">ESL</option>
                            </select>
                        </div>
                        <hr />
                    </div>



                <div id="courseSection" style="display: none;">
                    <div class="form-group">
                    <b class="text-primary">Course:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" value="1" onclick="displayRestSection()">
                        <label class="custom-control-label" for="customRadio1">Java 1</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" value="2" onclick="displayRestSection()">
                        <label class="custom-control-label" for="customRadio2">Java 2</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input" value="3" onclick="displayRestSection()">
                        <label class="custom-control-label" for="customRadio3">Java 3</label>
                    </div>
                    </div>
                    <hr />
                </div>

                <div id="restSection" style="display: none;">
                    <b class="text-primary">Tags:</b>
                    <br />
                    <br />
                    <div class="form-group">
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label" for="customCheck1">Variables</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                            <label class="custom-control-label" for="customCheck2">Data Type</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="customCheck3">
                            <label class="custom-control-label" for="customCheck3">Loops</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="customCheck4">
                            <label class="custom-control-label" for="customCheck4">If Statement</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="customCheck5">
                            <label class="custom-control-label" for="customCheck5">Exception</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="customCheck6">
                            <label class="custom-control-label" for="customCheck6">Array</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="customCheck7">
                            <label class="custom-control-label" for="customCheck7">Class</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="customCheck8">
                            <label class="custom-control-label" for="customCheck8">Object</label>
                        </div>
                    </div>
                    <hr />
                    <button class="btn btn-primary" onclick="displayQuestionSheet()">Generate</button>
                </div>
                </div>
            </div>

            <div id="questionListSection" style="margin-top:4%;display: none;" >
            <h3 class="text-primary" style="margin-bottom: 2%;"><b>Question Sheet</b></h3>
            <div class="card mb-2">
                <div class="card-body">
                    <div class="questionCard">
                        <p><b>1.</b> &nbsp;&nbsp; <span class="badge badge-pill badge-primary">If Statement</span> &nbsp;&nbsp; <span class="badge badge-pill badge-primary">Exception</span></p>
                        <p>Write Java program to allow the user to input his/her age. Then the program will show if the person is eligible to vote. A person who is eligible to vote must be older than or equal to 18 years old.

                            <br/>Enter your age: 18

                            <br/>You are eligible to vote.</p>
                        <div class="custom-control custom-checkbox custom-control-inline" style="left:90%;">
                            <input type="checkbox" class="custom-control-input" id="tooDifficult1">
                            <label class="custom-control-label text-primary" for="tooDifficult1" style="font-weight: bold;">Too Difficult</label>
                        </div>
                    </div>
                    <hr />
                    <div class="questionCard">
                        <p><b>2.</b> &nbsp;&nbsp; <span class="badge badge-pill badge-primary">Data Type</span> &nbsp;&nbsp; <span class="badge badge-pill badge-primary">Variables</span></p>
                        <p>The primitive data type _____ would be most appropriate for reflecting characters in Unicode.</p>
                        <div class="custom-control custom-checkbox custom-control-inline" style="left:90%;">
                            <input type="checkbox" class="custom-control-input" id="tooDifficult2">
                            <label class="custom-control-label text-primary" for="tooDifficult2" style="font-weight: bold;">Too Difficult</label>
                        </div>
                    </div>
                    <hr />
                    <div class="questionCard">
                        <p><b>3.</b> &nbsp;&nbsp; <span class="badge badge-pill badge-primary">Loop</span></p>
                        <p>Write a Java program by using three for loops to print the following pattern:<br/>
                            1******<br />
                            12*****<br />
                            123****<br />
                            1234***<br />
                            12345**<br />
                            123456*<br />
                            1234567<br />
                        </p>
                        <div class="custom-control custom-checkbox custom-control-inline" style="left:90%;">
                            <input type="checkbox" class="custom-control-input" id="tooDifficult3">
                            <label class="custom-control-label text-primary" for="tooDifficult3" style="font-weight: bold;">Too Difficult</label>
                        </div>
                    </div>
                    <hr />
                    <div class="questionCard">
                        <p><b>4.</b> &nbsp;&nbsp; <span class="badge badge-pill badge-primary">Array</span>
                        <p>Set up an array to hold the following values, and in this order: 23, 6, 47, 35, 2, 14. Write a programme to get the average of all 6 numbers. (You can use integers for this exercise, which will round down your answer.)</p>
                        <div class="custom-control custom-checkbox custom-control-inline" style="left:90%;">
                            <input type="checkbox" class="custom-control-input" id="tooDifficult4">
                            <label class="custom-control-label text-primary" for="tooDifficult4" style="font-weight: bold;">Too Difficult</label>
                        </div>
                    </div>
                    <hr />
                    <div class="questionCard">
                        <p><b>5.</b> &nbsp;&nbsp; <span class="badge badge-pill badge-primary">Array</span> &nbsp;&nbsp;<span class="badge badge-pill badge-primary">Loop</span>&nbsp;&nbsp;<span class="badge badge-pill badge-primary">Exception</span>
                        <table style="width:100%;">
                            <tr>
                                <td style="width:50%;"><img src="<c:url value="/images/exceptionQuestion.PNG"/>" alt="exceptionQuestion"></td>
                                <td style="width:50%; font-size: 15px;">The Program contains a bug. Find and fix it</td>
                            </tr>
                        </table>
                        <div class="custom-control custom-checkbox custom-control-inline" style="left:90%;">
                            <input type="checkbox" class="custom-control-input" id="tooDifficult5">
                            <label class="custom-control-label text-primary" for="tooDifficult5" style="font-weight: bold;">Too Difficult</label>
                        </div>
                    </div>
                    <hr />
                    <div class="questionCard">
                        <p><b>6.</b> &nbsp;&nbsp;<span class="badge badge-pill badge-primary">Exception</span>
                        <table style="width:100%;">
                            <tr>
                                <td style="width:50%;"><img src="<c:url value="/images/exceptionTwo.PNG"/>" alt="exceptionQuestion"></td>
                                <td style="width:50%; font-size: 15px;">Is there anything wrong with the following exception handler as written? Will this code compile?</td>
                            </tr>
                        </table>
                        <div class="custom-control custom-checkbox custom-control-inline" style="left:90%;">
                            <input type="checkbox" class="custom-control-input" id="tooDifficult6">
                            <label class="custom-control-label text-primary" for="tooDifficult6" style="font-weight: bold;">Too Difficult</label>
                        </div>
                    </div>
                    <hr />
                    <div class="questionCard">
                        <p><b>7.</b> &nbsp;&nbsp;<span class="badge badge-pill badge-primary">Data Type</span>
                        <table style="width:100%;">
                            <tr>
                                <td style="width:50%;"><img src="<c:url value="/images/dataTypeQuestion.PNG"/>" alt="exceptionQuestion"></td>
                                <td style="width:50%; font-size: 15px;">Write a test program to measure the execution time of sorting 100,000 numbers using selection sort</td>
                            </tr>
                        </table>
                        <div class="custom-control custom-checkbox custom-control-inline" style="left:90%;">
                            <input type="checkbox" class="custom-control-input" id="tooDifficult7">
                            <label class="custom-control-label text-primary" for="tooDifficult7" style="font-weight: bold;">Too Difficult</label>
                        </div>
                    </div>
                    <hr />
                    <div class="questionCard">
                        <p><b>8.</b> &nbsp;&nbsp;<span class="badge badge-pill badge-primary">If Statement</span>&nbsp;&nbsp;<span class="badge badge-pill badge-primary">Exception</span>
                        <table style="width:100%;">
                            <tr>
                                <td style="width:50%;"><img src="<c:url value="/images/ifQuestion.PNG"/>" alt="exceptionQuestion"></td>
                                <td style="width:50%; font-size: 15px;">What happens if the following code is compiled and run?</td>
                            </tr>
                        </table>
                        <div class="custom-control custom-checkbox custom-control-inline" style="left:90%;">
                            <input type="checkbox" class="custom-control-input" id="tooDifficult8">
                            <label class="custom-control-label text-primary" for="tooDifficult8" style="font-weight: bold;">Too Difficult</label>
                        </div>
                    </div>
                </div>

            </div>
            <div style="margin-top: 2%;">
            <button class="btn btn-primary">Downdown Question Sheet</button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-primary">Survey</button>
            </div>
            </div>
        </div>
    </div>
</div>

<!--Footer-->
<footer class="footer bg-light">
    <div class="container" id="footer-container">
        <span class="text-muted" style="font-size:20px;">@ 2018 Copyright: Tinkers</span>
    </div>
</footer>
</body>
</html>
