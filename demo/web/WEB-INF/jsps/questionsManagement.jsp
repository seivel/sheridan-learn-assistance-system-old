<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<c:url value="/scripts/bootstrap.min.js" />"></script>
    <script src="<c:url value="/scripts/script.js" />"></script>
    <script src="<c:url value="/scripts/angular.min.js" />"></script>
    <script src="<c:url value="/scripts/questionManagementScript.js" />"></script>
    <link rel="shortcut icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
    <link rel="stylesheet" href="<c:url value="/css/style.css"/> "/>
    <link rel="stylesheet" href="<c:url value="/css/adminHomeCss.css"/> "/>
    <link rel="stylesheet" href="<c:url value="/css/questionManagementCss.css"/> "/>
    <title>Questions Masnagement</title>
</head>
<body>
<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="#">Tools</a>
</div>

<div id="main" style="padding:0">
<div class="container-fluid" style="padding:0">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="<c:url value="/"/>" style="margin-left:5%; font-size:20px!important;"><img
                src='<c:url value="/images/sheridanLogo.png"/>' alt="sheridan logo" width="30px" height="40.797px"
                style="margin-right:20px;"/>Sheridan Learning Assistance System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <c:url value="/logout" var="logoutUrl"/>
            <form id="logout" action="${logoutUrl}" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
            <button class="dropdown btn btn-outline-secondary"  style="font-size:20px !important; margin-bottom:0; margin-left:80%; background:transparent; padding:0;border:none">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white; padding:0;">
                    <img src='<c:url value="/images/profileicon.png"/>' alt="profileicon" width="60px" height="60px"/> &nbsp;Admin</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-2px, 53%, 0px);">
                    <a class="dropdown-item" href="<c:url value="/resetPassword"/>">Reset the Password</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="logout()">Logout</a>
                </div>
            </button>
        </div>
    </nav>
    <div class="btn-group btn-group-toggle" id="tabsPanel">
        <a href="<c:url value="/examCram"/>" class="btn btn-primary panel-item">
            Exam Cram
        </a>
        <a href="<c:url value="/secure"/>" class="btn btn-primary panel-item">
            Accounts Management
        </a>
        <a  href="<c:url value="/questionsManagement"/>" class="btn active btn-primary panel-item">
            Questions Management
        </a>
        <a  href="<c:url value="/tagsManagement"/>" class="btn btn-primary panel-item">
            Tags Management
        </a>
        <a href="<c:url value="/reports"/>" class="btn btn-primary panel-item" >Reports</a>
    </div>

    <div ng-app="sortApp" ng-controller="mainController">
        <div id="bodyContent">
            <div class="jumbotron">
                <div>
                    <a href="#" class="btn btn-primary mb-4" data-toggle="modal" data-target="#addModal"  data-backdrop="static" data-keyboard="false">Add new Question</a> &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#" class="btn btn-primary mb-4" data-toggle="modal" data-target="#updateModal"  data-backdrop="static" data-keyboard="false">Update Question</a> &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#" class="btn btn-primary mb-4" data-toggle="modal" data-target="#disableModal">Disable Question</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="#" class="btn btn-primary mb-4"  onclick="openNav()">&#9776; Tool Bar</a>

                    <!--New Modal -->
                    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title text-primary"><b>Add new Question</b></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <c:url value="/saveQuestion" var="saveUrl" />
                                    <form:form modelAttribute="question" method="post" action="${saveUrl}" >
                                        <table class="table" width="100%">
                                            <tr>
                                                <td>
                                                    <b>Program: &nbsp;&nbsp; Computer Science</b>
                                                    <%--<div class="form-group">--%>
                                                        <%--<select class="custom-select" id="programSelection" onclick="displayCourseSection()">--%>
                                                            <%--<option selected>Choose the Program</option>--%>
                                                            <%--<c:forEach var="program" items="programList">--%>
                                                                <%--<form:option value="1">${program.program}</form:option>--%>
                                                            <%--</c:forEach>--%>
                                                            <%--&lt;%&ndash;<option value="1">Computer Science</option>&ndash;%&gt;--%>
                                                            <%--&lt;%&ndash;<option value="2">Animation Design</option>&ndash;%&gt;--%>
                                                            <%--&lt;%&ndash;<option value="3">ESL</option>&ndash;%&gt;--%>
                                                        <%--</select>--%>
                                                        <%--<form:select id="programSelection" class="custom-select" path="" onclick="displayCourseSection()">
                                                            <form:option value="">Choose the program</form:option>
                                                            <c:forEach var="program" items="${programList}">
                                                            <form:option value="${program.id}">${program.program}</form:option>
                                                            </c:forEach>
                                                        </form:select>--%>


                                                    <%--</div>--%>
                                                </td>
                                            </tr>
                                            <%--<tr id="courseSection" style="display: none;">--%>
                                            <tr id="courseSection">
                                                <td>
                                                    <b>Course:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                    <c:forEach var="course" items="${courseList}">
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <form:radiobutton path="courseSet" value="${course.course}" id="${course.course}" class="custom-control-input" required="required" />
                                                            <label class="custom-control-label" for="${course.course}">${course.course}</label>
                                                        </div>
                                                    </c:forEach>
                                                    <%--<div class="custom-control custom-radio custom-control-inline">--%>
                                                        <%--<input type="radio" id="newCourseRadio1" name="newCourseRadio" class="custom-control-input" value="1" onclick="displayRestSection()">--%>
                                                        <%--<label class="custom-control-label" for="newCourseRadio1">Java 1</label>--%>
                                                    <%--</div>--%>
                                                    <%--<div class="custom-control custom-radio custom-control-inline">--%>
                                                        <%--<input type="radio" id="newCourseRadio2" name="newCourseRadio" class="custom-control-input" value="2" onclick="displayRestSection()">--%>
                                                        <%--<label class="custom-control-label" for="newCourseRadio2">Java 2</label>--%>
                                                    <%--</div>--%>
                                                    <%--<div class="custom-control custom-radio custom-control-inline">--%>
                                                        <%--<input type="radio" id="newCourseRadio3" name="newCourseRadio" class="custom-control-input" value="3" onclick="displayRestSection()">--%>
                                                        <%--<label class="custom-control-label" for="newCourseRadio3">Java 3</label>--%>
                                                    <%--</div>--%>
                                                </td>
                                            </tr>
                                            <%--<tr class="restSection"  style="display: none;">--%>
                                            <tr class="restSection">
                                                <td>
                                                    <b>Tags:</b>
                                                    <br />
                                                    <br />
                                                    <div class="form-group">

                                                        <c:forEach var="tag" items="${tagList}">
                                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                                <form:checkbox path="tagSet" value="${tag.tag}" id="${tag.id}" class="custom-control-input" />
                                                                <label class="custom-control-label" for="${tag.id}">${tag.tag}</label>
                                                            </div>
                                                        </c:forEach>
                                                        <%--<div class="custom-control custom-checkbox custom-control-inline">--%>
                                                            <%--<input type="checkbox" class="custom-control-input" id="newTagCheck1">--%>
                                                            <%--<label class="custom-control-label" for="newTagCheck1">Variables</label>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="custom-control custom-checkbox custom-control-inline">--%>
                                                            <%--<input type="checkbox" class="custom-control-input" id="newTagCheck2">--%>
                                                            <%--<label class="custom-control-label" for="newTagCheck2">Data Type</label>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="custom-control custom-checkbox custom-control-inline">--%>
                                                            <%--<input type="checkbox" class="custom-control-input" id="newTagCheck3">--%>
                                                            <%--<label class="custom-control-label" for="newTagCheck3">Loops</label>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="custom-control custom-checkbox custom-control-inline">--%>
                                                            <%--<input type="checkbox" class="custom-control-input" id="newTagCheck4">--%>
                                                            <%--<label class="custom-control-label" for="newTagCheck4">If Statement</label>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="custom-control custom-checkbox custom-control-inline">--%>
                                                            <%--<input type="checkbox" class="custom-control-input" id="newTagCheck5">--%>
                                                            <%--<label class="custom-control-label" for="newTagCheck5">Exception</label>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="custom-control custom-checkbox custom-control-inline">--%>
                                                            <%--<input type="checkbox" class="custom-control-input" id="newTagCheck6">--%>
                                                            <%--<label class="custom-control-label" for="newTagCheck6">Array</label>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="custom-control custom-checkbox custom-control-inline">--%>
                                                            <%--<input type="checkbox" class="custom-control-input" id="newTagCheck7">--%>
                                                            <%--<label class="custom-control-label" for="newTagCheck7">Class</label>--%>
                                                        <%--</div>--%>
                                                        <%--<div class="custom-control custom-checkbox custom-control-inline">--%>
                                                            <%--<input type="checkbox" class="custom-control-input" id="newTagCheck8">--%>
                                                            <%--<label class="custom-control-label" for="newTagCheck8">Object</label>--%>
                                                        <%--</div>--%>
                                                    </div>
                                                </td>
                                            </tr>
                                            <%--<tr class="restSection"  style="display: none;">--%>
                                            <tr class="restSection">
                                                <td>
                                                    <b>Difficulty Level:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <form:radiobutton path="difficulty" id="newLevel1" name="newLevelRadio" class="custom-control-input" value="1" required="required"/>
                                                        <label class="custom-control-label" for="newLevel1">Easy</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <form:radiobutton path="difficulty" id="newLevel2" name="newLevelRadio" class="custom-control-input" value="2" required="required"/>
                                                        <label class="custom-control-label" for="newLevel2">Normal</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <form:radiobutton path="difficulty" id="newLevel3" name="newLevelRadio" class="custom-control-input" value="3" required="required"/>
                                                        <label class="custom-control-label" for="newLevel3">Difficult</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <%--<tr class="restSection"  style="display: none;">--%>
                                            <tr class="restSection">
                                                <td>
                                                    <b>Description:</b>
                                                    <br />
                                                    <br />
                                                    <form:textarea path="description" class="form-control" rows="5" id="newDescription" required="required"/>
                                                    <%--<textarea class="form-control" rows="5" id="newDescription"></textarea>--%>
                                                </td>
                                            </tr>
                                            <%--<tr class="restSection"  style="display: none;">--%>
                                            <%--<tr class="restSection">--%>
                                                <%--<td>--%>
                                                    <%--<b>Images:</b>--%>
                                                    <%--<br />--%>
                                                    <%--<br />--%>
                                                    <%--<div class="form-group">--%>
                                                        <%--<div class="input-group mb-3">--%>
                                                            <%--<div class="custom-file">--%>
                                                                <%--<input type="file" class="custom-file-input" id="newinputGroupFile02">--%>
                                                                <%--<label class="custom-file-label" for="newinputGroupFile02">Choose file</label>--%>
                                                            <%--</div>--%>
                                                        <%--</div>--%>
                                                    <%--</div>--%>
                                                <%--</td>--%>
                                            <%--</tr>--%>
                                        </table>
                                        <%--<div class="modal-footer restSection"  style="display: none;">--%>
                                        <div class="modal-footer restSection">
                                            <input type="submit" class="btn btn-primary" value="Add">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" value="">Close</button>
                                        </div>
                                    </form:form>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!--Update Modal -->
                    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title text-primary"><b>Update Question</b></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="#">
                                        <table class="table" width="100%">
                                            <tr>
                                                <td>
                                                    <b>Program:</b>
                                                    <br />
                                                    <br />

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Course:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="customRadio1" name="customRadioCourse" class="custom-control-input" checked>
                                                            <label class="custom-control-label" for="customRadio1">Java 1</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="customRadio2" name="customRadioCourse" class="custom-control-input">
                                                            <label class="custom-control-label" for="customRadio2">Java 2</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="customRadio3" name="customRadioCourse" class="custom-control-input">
                                                            <label class="custom-control-label" for="customRadio3">Java 3</label>
                                                        </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Tags:</b>
                                                    <br />
                                                    <br />
                                                    <div class="form-group">
                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                            <label class="custom-control-label" for="customCheck1">Variables</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                                                            <label class="custom-control-label" for="customCheck2">Data Type</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck3">
                                                            <label class="custom-control-label" for="customCheck3">Loops</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck4" checked="">
                                                            <label class="custom-control-label" for="customCheck4">If Statement</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck5" checked="">
                                                            <label class="custom-control-label" for="customCheck5">Exception</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck6">
                                                            <label class="custom-control-label" for="customCheck6">Array</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck7">
                                                            <label class="custom-control-label" for="customCheck7">Class</label>
                                                        </div>
                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck8">
                                                            <label class="custom-control-label" for="customCheck8">Object</label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Difficulty Level:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="level1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="level1">Easy</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="level2" name="customRadio" class="custom-control-input" checked>
                                                        <label class="custom-control-label" for="level2">Normal</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="leve3" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="leve3">Difficult</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Description:</b>
                                                    <br />
                                                    <br />
                                                    <textarea class="form-control" rows="5" id="description">Write Java program to allow the user to input his/her age. Then the program will show if the person is eligible to vote. A person who is eligible to vote must be older than or equal to 18 years old.</textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Images:</b>
                                                    <br />
                                                    <br />
                                                    <div class="form-group">
                                                        <div class="input-group mb-3">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" id="inputGroupFile02">
                                                                <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" value="">Close</button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!--Disable Modal -->
                    <div class="modal fade" id="disableModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title text-primary" id="exampleModalLabel"><b>Disable Questions</b></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Confirm to disable all selected questions?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                    <button type="button" class="btn btn-primary">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                <input type="text" class="form-control" placeholder="Type Text to Search" ng-model="test">
                            </div>
                        </div>
                    </form>

                    <div class="card mb-2">
                        <div class="card-body">
                            <table class="table table-bordered table-striped" style="font-size:15px;">
                                <thead>
                                <tr>
                                    <td></td>

                                    <td>
                                        <%--ID--%>
                                        <a href="#" ng-click="sortType = 'ID'">
                                            ID
                                            <span ng-show="sortType == 'ID'" class="fa fa-caret-down"></span>
                                        </a>

                                    </td>
                                    <td>
                                        <a href="#" ng-click="sortType = 'course'">
                                            Course
                                            <span ng-show="sortType == 'course'" class="fa fa-caret-down"></span>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" ng-click="sortType = 'tags'">
                                            Tags
                                            <span ng-show="sortType == 'tags'" class="fa fa-caret-down"></span>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" ng-click="sortType = 'program'">
                                            Image
                                            <span ng-show="sortType == 'program'" class="fa fa-caret-down"></span>
                                        </a>
                                    </td>

                                    <td>
                                        <a href="#" ng-click="sortType = 'description'">
                                            Description
                                            <span ng-show="sortType == 'description'" class="fa fa-caret-down"></span>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" ng-click="sortType = 'lastEditBy'">
                                            Last Edit
                                            <span ng-show="sortType == 'lastEditBy'" class="fa fa-caret-down"></span>
                                        </a>
                                    </td>
                                    <td>
                                        Available
                                    </td>
                                </tr>
                                </thead>

                                <tbody>
                                <c:forEach var="question" items="${questionList}">

                                    <tr>
                                        <td><input type="checkbox" value="${question.id}" /></td>
                                        <td>${question.id}</td>
                                        <td>
                                            <c:forEach var="courseSet" items="${question.courseSet}">
                                                ${courseSet.course}
                                            </c:forEach>
                                        </td>
                                        <td>
                                            <c:forEach var="tagSet" items="${question.tagSet}">
                                                ${tagSet.tag}
                                            </c:forEach>
                                        </td>
                                        <td>
                                            <%--<c:forEach var="courseSet" items="${question.courseSet}">--%>
                                                <%--${courseSet.program.program}--%>
                                            <%--</c:forEach>--%>
                                            <c:forEach var="imageSet" items="${question.imageSet}">
                                                ${imageSet.fileName}
                                            </c:forEach>
                                        </td>
                                        <td>${question.description}</td>
                                        <td>${question.updatedBy}<br/>${question.updateAt}</td>
                                        <td>${question.available}</td>
                                    </tr>

                                </c:forEach>
                                <%--<tr ng-repeat="item in question | orderBy:sortType:sortReverse | filter:test">
                                    <td><input type="checkbox"/></td>
                                    <td>{{item.id}}</td>
                                    <td>{{item.course}}</td>
                                    <td>{{item.tags}}</td>
                                    <td>{{item.program}}</td>
                                    <td>{{item.image}}</td>
                                    <td>{{item.description}}</td>
                                    <td>{{item.lastEditBy}}</td>
                                </tr>--%>
                                </tbody>
                            </table>

                            <!-- Modal -->
                            <div style="width: 20%;margin: auto;margin-top: 3%;">
                                <ul class="pagination">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#">&laquo;</a>
                                    </li>
                                    <li class="page-item active">
                                        <a class="page-link" href="#">1</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">2</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">3</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">4</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">5</a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#">&raquo;</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!--Footer-->
    <footer class="footer bg-light">
        <div class="container" id="footer-container">
            <span class="text-muted" style="font-size:20px;">@ 2018 Copyright: Tinkers</span>
        </div>
    </footer>
</div>
</div>
</body>
</html>
