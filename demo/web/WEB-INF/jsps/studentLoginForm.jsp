<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Student Login</title>
    <link rel="shortcut icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <script src="<c:url value="/scripts/bootstrap.min.js" />"></script>
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
    <style>
        #error {
            color:red;
        }
        footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            text-align: center;
            height: 6%;
        }
        #footer-container{
            margin-top:1%;
        }
        #contentBody{
            width:90%;
            margin:auto;
            margin-top:1%;
        }
        .borderless td, .borderless th {
            border: none;
        }
        table{
            text-align:center;
        }

        form{
            margin-top:10%;
        }

        td
        {
            width:50%!important;
        }

        #loginFormTable{
            width:100%!important;
            font-size: 20px;
            margin-top:5%;
        }
        #loginFormTable td{
            padding-top:5%!important;
        }
        #loginFormContainer{
            width:70%;
            margin: auto;
            padding-top:15%;
            padding-bottom:5%;
            margin-top:5%;
            border: 7px outset steelblue
        }
    </style>
</head>
<body>
<div class="container-fluid" style="padding:0">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="<c:url value="/"/>" style="margin-left:5%"><img src='<c:url value="/images/sheridanLogo.png"/>' alt="sheridan logo" width="30px" height="40.797px" style="margin-right:20px;"/>Sheridan Learning Assistance System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">

            <p style="font-size:20px !important; margin-bottom:0; margin-left:75%" class="text-white">Student Login</p>

        </div>
    </nav>
</div>

<div id="contentBody">
    <table class="table borderless">
        <tr>
            <td>
                <h1 style="margin-top:25%;"><b>Notice</b></h1>
                <p style="font-size:20px!important;">Lorem ipsum dolor sit amet, sea modus putent cu, ei mei causae vituperatoribus, usu te percipit quaestio. Id facete minimum his, sea ut purto omittam efficiendi. Vim reque molestiae no. Nostro nostrum consulatu eu eos, mea ad odio utamur ancillae. Eirmod dolorem repudiare te mei, est ut noluisse senserit. Usu definitiones necessitatibus ad, nec melius minimum menandri ut, solet complectitur ne mei. Vix tale quas tibique no, et natum ludus quaeque pro.

                    Vis ex singulis delicatissimi, denique albucius menititum est in. Vis nostro nominavi consectetuer eu, cu quod aperiri sea, ea dicat accusamus vel. Eam id mazim tantas. Homero quidam expetendis has ne. Graecis singulis consequuntur eum et. Id utroque antiopam tractatos sea. Est purto altera intellegam ex.</p>
            </td>
            <td>
                <div id="loginFormContainer">
                    <h1 style="margin-top:8%;"><b>Student Login</b></h1>
                    <form method="get" action="<c:url value="/studentExamCram"/>">
                        <table id="loginFormTable">
                            <tr>
                                <td><b>Name:</b></td>
                                <td><input type="text" name="username" /></td>
                            </tr>
                            <tr>
                                <td><b>Student ID:</b></td>
                                <td><input type="password" name="password" /><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /></td>

                            </tr>
                            <tr>
                                <td><b>Access Code:</b></td>
                                <td><input type="password" name="accessCode" /></td>
                            </tr>
                            <tr>
                                <td><b>Campus:</b></td>
                                <td> <label>
                                    <input type="radio" name="DAV" value="always"/>DAV
                                </label>
                                    <label>
                                        <input type="radio" name="HMC" value="never"/>HMC
                                    </label>
                                    <label>
                                        <input type="radio" name="TRA" value="costChange"/>TRA
                                    </label></td>
                            </tr>
                        </table>

                        <br />
                        <br />
                        <input class="btn btn-primary btn-lg" type="submit" value="Login"/>
                    </form>
                </div>
            </td>
        <tr>
    </table>

</div>

<!--Footer-->
<footer class="footer bg-light">
    <div class="container" id="footer-container">
        <span class="text-muted" style="font-size:20px;">@ 2018 Copyright: Tinkers</span>
    </div>
</footer>
</body>
</html>
