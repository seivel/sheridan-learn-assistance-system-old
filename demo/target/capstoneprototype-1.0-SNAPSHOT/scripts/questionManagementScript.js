/*
angular.module('sortApp',[])

    .controller('mainController',function($scope){
        $scope.sortType = 'id';
        $scope.sortReverse = false;
        $scope.searchFish = '';

        $scope.question = [
            {id: '1', course:'Java 1',tags:'If Statement, Exception',program:"Computer Science",image: 'No', description:"Write Java program to allow the user to input his/her age. Then the program will show if the person is eligible to vote. A person who is eligible to vote must be older than or equal to 18 years old.", lastEditBy:"Rich Smith"},
            {id: '5', course:'Java 2',tags:'inheritance, Generic Type',program:"Computer Science",image: 'Fruit.jpg', description:"In the example below, Apple inherits from Fruit. This is specified with the extends keyword. Fruit then becomes the superclass of Apple, which in turn becomes a subclass of Fruit.", lastEditBy:"Zhuo Zhnang"},
            {id: '12', course:'Java 2',tags:'Multi-Dimensional Arrays',program:"Computer Science",image: 'No', description:"Write a method\n" +
                "public static boolean isLatin(int[][] a)\n" +
                "that checks to see if the array is a Latin square. This means that it must be square (suppose it is n x n), and that each row and each column must contain the values 1, 2, ..., n with no repeats.\n" +
                "Some helper methods you may need for this example and the next are suggested in the tester code. Uncomment and run these tests if you implement these helper methods.", lastEditBy:"Rich Smith"},
            {id: '22', course:'Java 2',tags:'Exception, ArrayList, DataStructure',program:"Computer Science",image: 'No', description:"How do you decrease the current capacity of an ArrayList to the current size?", lastEditBy:"Brandon Church"},
            {id: '35', course:'Java 1',tags:'data type, variables, scope',program:"Computer Science",image: 'No', description:"The primitive data type _____ would be most appropriate for reflecting characters in Unicode.", lastEditBy:"Eric Li"},
            {id: '51', course:'Java 3',tags:'multi-thread, JUnit Test',program:"Computer Science",image: 'FormExample.jpg', description:"Describe how you start up a JUnit test", lastEditBy:"Jason Butterfield"},
            {id: '66', course:'Java 3',tags:'Spring Form',program:"Computer Science",image: 'No', description:"Develop a form which will CRUD Movies", lastEditBy:"Latus An"},
        ];
    });
*/

var programValue
var programList

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}

// function displayCourseSection(){
//     programValue = $("#programSelection").val();
// /*    if(programValue == 1){
//         $("#courseSection").css("display","block");
//     }else{
//         $("#courseSection").css("display","none");
//         $(".restSection").css("display","none");
//     }*/
//
//
// }
//
// function displayRestSection(){
//     var programValue = $("#programSelection").val();
//     var courseValue = $("input[name=newCourseRadio]:checked").val();
//
//     if(programValue == 1 && courseValue == 1){
//         $(".restSection").css("display","block");
//     }else{
//         $(".restSection").css("display","none");
//     }
// }