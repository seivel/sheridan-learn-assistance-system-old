<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Question Editor</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="<c:url value="/scripts/bootstrap.min.js" />"></script>
	<script src="<c:url value="/scripts/script.js" />"></script>
	<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
	<link rel="stylesheet" href="<c:url value="/css/style.css"/> "/>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<a class="navbar-brand" href="#" style="margin-left:5%; font-size:20px!important;"><img
				src='<c:url value="/images/sheridanLogo.png"/>' alt="sheridan logo" width="30px" height="40.797px"
				style="margin-right:20px;"/>Sheridan Learning Assistance System</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
		        aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
	</nav>

	<div class="container">

		<div class="jumbotron">

			<a href="/secure">View All Question</a>

			<c:url var="saveURL" value="/saveQuestion"/>
			<form:form modelAttribute="question" method="post" action="${saveURL}">
				<fieldset>
					<legend>Question Editor</legend>
					<div class="form-group">
						<label for="courseSet">Course</label>
						<c:forEach var="c" items="${course}">
							<div class="form-check">
								<label class="form-check-label">
									<input name="courseSet" id="courseSet" class="form-check-input"
									       type="checkbox" value="${c.course}">
										${c.course}
								</label>
							</div>
						</c:forEach>
					</div>

					<div class="form-group">
						<label for="tagSet">Tag</label>
						<c:forEach var="t" items="${tags}">
							<div class="form-check">
								<label class="form-check-label">
									<input id="tagSet" name="tagSet" class="form-check-input"
									       type="checkbox" value="${t.tag}">
										${t.tag}
								</label>
							</div>
						</c:forEach>
					</div>

					<div class="form-group">
						<label>Question Description</label>
					</div>
					<div class="form-group">
						<form:textarea path="description" cols="60" rows="30" cssClass="form-control"/>
					</div>

					<div class="form-group">
						<div>
							<select name="difficulty" class="form-control">
								<option value="-1">Select Difficulty</option>
								<option value="1">Easy</option>
								<option value="2">Med</option>
								<option value="3">Hard</option>
							</select>
						</div>
					</div>

					<input type="submit" value="Save" class="btn btn-primary"/>

				</fieldset>
			</form:form>

		</div>
	</div>

	<footer class="footer bg-light">
		<div class="container" id="footer-container">
			<span class="text-muted" style="font-size:20px;">@ 2018 Copyright: Tinkers</span>
		</div>
	</footer>
</body>
</html>
