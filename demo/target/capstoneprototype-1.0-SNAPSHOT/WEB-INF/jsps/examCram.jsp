<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<c:url value="/scripts/moment.js" />"></script>
    <script src="<c:url value="/scripts/bootstrap-datetimepicker.min.js" />"></script>
    <script src="<c:url value="/scripts/bootstrap.min.js" />"></script>
    <script src="<c:url value="/scripts/reportScript.js" />"></script>
    <link rel="shortcut icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
    <link rel="stylesheet" href="<c:url value="/css/jquery-ui.min.css" />">
    <link rel="stylesheet" href="<c:url value="/css/examCramCss.css" />">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap-datetimepicker.min.css" />">
    <title>Exam Cram</title>
</head>
<body>
<div class="container-fluid" style="padding:0">
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="<c:url value="/"/>" style="margin-left:5%; font-size:20px!important;"><img
                src='<c:url value="/images/sheridanLogo.png"/>' alt="sheridan logo" width="30px" height="40.797px"
                style="margin-right:20px;"/>Sheridan Learning Assistance System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <c:url value="/logout" var="logoutUrl"/>
            <form id="logout" action="${logoutUrl}" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
            <button class="dropdown btn btn-outline-secondary"  style="font-size:20px !important; margin-bottom:0; margin-left:80%; background:transparent; padding:0;border:none">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white; padding:0;">
                    <img src='<c:url value="/images/profileicon.png"/>' alt="profileicon" width="60px" height="60px"/> &nbsp;Admin</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-2px, 53%, 0px);">
                    <a class="dropdown-item" href="<c:url value="/resetPassword"/>">Reset the Password</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" onclick="logout()">Logout</a>
                </div>
            </button>
        </div>
    </nav>
    <div class="btn-group btn-group-toggle" id="tabsPanel">
        <a href="<c:url value="/examCram"/>" class="btn active btn-primary panel-item">
            Exam Cram
        </a>
        <a href="<c:url value="/secure"/>" class="btn btn-primary panel-item">
            Accounts Management
        </a>
        <a  href="<c:url value="/questionsManagement"/>" class="btn btn-primary panel-item">
            Questions Management
        </a>
        <a  href="<c:url value="/tagsManagement"/>" class="btn btn-primary panel-item">
            Tags Management
        </a>
        <a href="<c:url value="/reports"/>" class="btn btn-primary panel-item" >Reports</a>
    </div>
</div>
<div id="bodyContent">
    <div class="jumbotron">
        <div>
            <h3 class="text-primary" id="currentCramTitle"><b>Current Running Exam Cram</b></h3>
            <div class="card mb-2">
                <div class="card-body">
                    <table width="100%" style="font-size:18px;margin-bottom: 1%;">
                        <tr>
                            <td><b>Start:</b> 2018-04-27 6:00:00</td>
                            <td><b>End:</b> 2018-04-27 18:00:00 </td>
                            <td><b>Access Code: </b>aCCessTest</td>
                            <td style="float:right"><b>76%</b></td>
                        </tr>
                    </table>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                    </div>
                </div>
            </div>
            <h3 class="text-primary" id="futureCramTitle"><b>Next Exam Crams</b></h3>
            <div class="card mb-2">
                <div class="card-body">
                    <table width="100%" style="font-size:18px;margin-bottom: 1%;" class="table-hover table">
                        <tr>
                            <td><b>Start:</b> 2018-04-30 6:00:00</td>
                            <td><b>End:</b> 2018-04-30 18:00:00 </td>
                            <td><b>Access Code: </b>iwm3S</td>
                            <td><button class="btn btn-danger">Discard</button></td>
                        </tr>
                        <tr>
                            <td><b>Start:</b> 2018-05-01 6:00:00</td>
                            <td><b>End:</b> 2018-05-01 18:00:00 </td>
                            <td><b>Access Code: </b>dwas1739</td>
                            <td ><button class="btn btn-danger">Discard</button></td>
                        </tr>
                        <tr>
                            <td><b>Start:</b> 2018-05-04 6:00:00</td>
                            <td><b>End:</b> 2018-05-14 24:00:00 </td>
                            <td><b>Access Code: </b>seq89r</td>
                            <td ><button class="btn btn-danger">Discard</button></td>
                        </tr>
                        <tr>
                            <td><b>Start:</b> 2018-05-20 6:00:00</td>
                            <td><b>End:</b> 2018-05-22 18:00:00 </td>
                            <td><b>Access Code: </b>ftoqm3B</td>
                            <td ><button class="btn btn-danger">Discard</button></td>
                        </tr>
                    </table>
                </div>
            </div>
            <h3 class="text-primary" id="newCramTitle"><b>Add New Exam Cram</b></h3>
            <div class="card mb-2">
                <div class="card-body">
                    <table width="96%" style="font-size:16px;margin-bottom: 1%;">
                        <tr>
                            <td><b>Start: </b><input class="datetimepicker" type="datetime-local"></td>
                            <td><b>End: </b><input class="datetimepicker" type="datetime-local"></td>
                            <td><b>Access Code: </b><input type="text"></td>
                            <td><button class="btn btn-primary">Confirm</button> </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>


<!--Footer-->
<footer class="footer bg-light">
    <div class="container" id="footer-container">
        <span class="text-muted" style="font-size:20px;">@ 2018 Copyright: Tinkers</span>
    </div>
</footer>
</body>
</html>
