<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="<c:url value="/scripts/bootstrap.min.js" />"></script>
	<script src="<c:url value="/scripts/script.js" />"></script>
	<script src="<c:url value="/scripts/angular.min.js" />"></script>
	<script src="<c:url value="/scripts/adminHomeScript.js" />"></script>
	<link rel="shortcut icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
	<link rel="icon" href="<c:url value="/images/favicon.png" />" type="image/x-icon">
	<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
	<link rel="stylesheet" href="<c:url value="/css/style.css"/> "/>
	<link rel="stylesheet" href="<c:url value="/css/adminHomeCss.css"/> "/>
	<title>Admin Home Page</title>
</head>
<body>
	<div class="container-fluid" style="padding:0">
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
			<a class="navbar-brand" href="<c:url value="/"/>" style="margin-left:5%; font-size:20px!important;"><img
					src='<c:url value="/images/sheridanLogo.png"/>' alt="sheridan logo" width="30px" height="40.797px"
					style="margin-right:20px;"/>Sheridan Learning Assistance System</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
			        aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarColor01">
				<c:url value="/logout" var="logoutUrl"/>
				<form id="logout" action="${logoutUrl}" method="post">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				</form>
				<button class="dropdown btn btn-outline-secondary"  style="font-size:20px !important; margin-bottom:0; margin-left:80%; background:transparent; padding:0;border:none">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: white; padding:0;">
						<img src='<c:url value="/images/profileicon.png"/>' alt="profileicon" width="60px" height="60px"/> &nbsp;Admin</a>
					<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-2px, 53%, 0px);">
						<a class="dropdown-item" href="<c:url value="/resetPassword"/>">Reset the Password</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#" onclick="logout()">Logout</a>
					</div>
				</button>
			</div>
		</nav>
		<div class="btn-group btn-group-toggle" id="tabsPanel">
			<a href="<c:url value="/examCram"/>" class="btn btn-primary panel-item">
				Exam Cram
			</a>
			<a href="<c:url value="/secure"/>" class="btn active btn-primary panel-item">
				Accounts Management
			</a>
			<a  href="<c:url value="/questionsManagement"/>" class="btn btn-primary panel-item">
				Questions Management
			</a>
			<a  href="<c:url value="/tagsManagement"/>" class="btn btn-primary panel-item">
				Tags Management
			</a>
			<a href="<c:url value="/reports"/>" class="btn btn-primary panel-item" >Reports</a>
		</div>
	</div>
	<div ng-app="sortApp" ng-controller="mainController">
		<div id="bodyContent">
			<div class="jumbotron">
				<div>
					<a href="#" class="btn btn-primary mb-4" data-toggle="modal" data-target="#exampleModal">Add new Account</a>
					<form>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-search"></i></div>
								<input type="text" class="form-control" placeholder="Type Text to Search" ng-model="test">
							</div>
						</div>
					</form>
					<div class="card mb-2">
						<div class="card-body">
								<table class="table table-bordered table-striped" style="font-size:15px;">
									<thead>
										<tr>
											<td>
												<a href="#" ng-click="sortType = 'username'">
												Username (Click to Edit)
												<span ng-show="sortType == 'username'" class="fa fa-caret-down"></span>
												</a>
											</td>
											<td>
												<a href="#" ng-click="sortType = 'name'">
												Name
												<span ng-show="sortType == 'name'" class="fa fa-caret-down"></span>
												</a>
											</td>
											<td>
												<a href="#" ng-click="sortType = 'email'">
												Email Address
												<span ng-show="sortType == 'email'" class="fa fa-caret-down"></span>
												</a>
											</td>
											<td>
												<a href="#" ng-click="sortType = 'role'">
												Role
												<span ng-show="sortType == 'role'" class="fa fa-caret-down"></span>
												</a>
											</td>
											<td>
												<a href="#" ng-click="sortType = 'disabled'">
												Enabled
												<span ng-show="sortType == 'disabled'" class="fa fa-caret-down"></span>
												</a>
											</td>
										</tr>
									</thead>
									<tbody>
										<%--<tr ng-repeat="account in user | orderBy:sortType:sortReverse | filter:test">
											<td><a href="#" data-toggle="modal" data-target="#exampleModalDetail">{{account.username}}</a></td>
											<td>{{account.name}}</td>
											<td>{{account.email}}</td>
											<td>{{account.role}}</td>
											<td>{{account.disabled}}</td>
										</tr>--%>
										<c:forEach var="user" items="${userList}">
											<tr>
												<td value="${user.username}">${user.username}</td>
												<td>${user.firstName} ${user.lastName}</td>
												<td>${user.email}</td>
												<td>
													<%--<c:forEach var="userRole" items="${user.roleSet}">
														${userRole.id}
													</c:forEach>--%>
													${user.roleSet.role}
														</td>
												<td>${user.enabled}</td>
											</tr>
										</c:forEach>

									</tbody>
								</table>

							<!-- Modal -->
							<div style="width: 20%;margin: auto;margin-top: 3%;">
								<ul class="pagination">
									<li class="page-item disabled">
										<a class="page-link" href="#">&laquo;</a>
									</li>
									<li class="page-item active">
										<a class="page-link" href="#">1</a>
									</li>
									<li class="page-item">
										<a class="page-link" href="#">2</a>
									</li>
									<li class="page-item">
										<a class="page-link" href="#">3</a>
									</li>
									<li class="page-item">
										<a class="page-link" href="#">4</a>
									</li>
									<li class="page-item">
										<a class="page-link" href="#">5</a>
									</li>
									<li class="page-item">
										<a class="page-link" href="#">&raquo;</a>
									</li>
								</ul>
							</div>

						</div>
						</div>
					</div>

				<div class="modal fade" id="exampleModalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title text-primary"><b>Update Account</b></h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form action="#">
									<table class="table" width="100%">
										<tr>
											<td><label class="col-form-label" for="firstNameFilled"><b>First Name: </b></label> </td>
											<td><input type="text" class="form-control-sm" id="firstNameFilled" value="Brandon"></td>
											<td><label class="col-form-label" for="lastNameFilled"><b>Last Name: </b></label> </td>
											<td><input type="text" class="form-control-sm" id="lastNameFilled" value="Church"></td>
										</tr>
										<tr>
											<td><label class="col-form-label" for="userNameFilled"><b>User Name: </b></label> </td>
											<td><input type="text" class="form-control-sm" id="userNameFilled" value="chUrchB"></td>
											<td><label class="col-form-label" for="emailAddressFilled"><b>Email Address: </b></label> </td>
											<td><input type="email" class="form-control-sm" id="emailAddressFilled" value="chruchbrandon@live.ca"></td>
										</tr>
										<tr>
											<td colspan="2" width="70%">
												<label class="col-form-label" for="passwordFilled"> <b>Password:</b> </label><br /><input type="password" class="form-control-sm" id="passwordFilled" style="width:100%">
												<label class="col-form-label" for="reEnterPasswordFilled"> <b>Re-enter Password:</b> </label><br /><input type="password" class="form-control-sm" id="reEnterPasswordFilled" style="width:100%">
											</td>
											<td colspan="2">
												<legend>Account Situation</legend>
												<div class="form-group">
													<div class="custom-control custom-radio">
														<input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
														<label class="custom-control-label" for="customRadio1">Enabled</label>
													</div>
													<br />
													<br />
													<div class="custom-control custom-radio">
														<input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" checked="">
														<label class="custom-control-label" for="customRadio2">Disabled</label>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="1"><b>Account Type:</b></td>
											<td colspan="3">
												<div style="margin-left:25%">
													<label class="form-check-label" style="margin-left:10%">
														<input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadiosFiiled1" value="option1" checked="">
														Professor
													</label>


													<label class="form-check-label" style="margin-left:10%">
														<input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadiosFilled2" value="option2">
														Learning Assistant
													</label>


													<label class="form-check-label" style="margin-left:10%">
														<input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadiosFiled3" value="option3">
														Admin
													</label>
												</div>
											</td>
										</tr>
									</table>
									<div class="modal-footer">
										<button type="submit" class="btn btn-primary">Update</button>
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									</div>
								</form>
							</div>

						</div>
					</div>
				</div>

<%--					<a href="<c:url value="/addQuestion"/>" class="btn btn-primary mb-4">Add new question</a>

					<c:forEach var="q" items="${questionList}">
						<div class="card mb-2">
							<div class="card-body">

								<c:forEach var="courseSet" items="${q.courseSet}">
									<span class="badge badge-pill badge-primary">${courseSet.course}</span>
								</c:forEach>

								<span class="badge badge-pill badge-secondary">
								<c:choose>
									<c:when test="${q.difficulty == 1}">
										Easy
									</c:when>
									<c:when test="${q.difficulty == 2}">
										Med
									</c:when>
									<c:otherwise>
										Hard
									</c:otherwise>
								</c:choose>
								</span>

								<c:forEach var="tagSet" items="${q.tagSet}">
									<span class="badge badge-pill badge-secondary">${tagSet.tag}</span>
								</c:forEach>

								<p class="card-body blockquote mb-0">
										${q.description}
								</p>

								<p>
									<a href="/editQuestion/${q.id}" class="btn btn-primary">Edit</a>
									<a href="/disableQuestion/${q.id}" class="btn btn-primary">Disable</a>
								</p>
							</div>
						</div>
					</c:forEach>--%>
				</div>
			</div>

			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title text-primary" id="exampleModalLabel"><b>Add New Account</b></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form action="#">
								<table class="table" width="100%">
									<tr>
										<td><label class="col-form-label" for="firstName"><b>First Name: </b></label> </td>
										<td><input type="text" class="form-control-sm" id="firstName"></td>
										<td><label class="col-form-label" for="lastName"><b>Last Name: </b></label> </td>
										<td><input type="text" class="form-control-sm" id="lastName"></td>
									</tr>
									<tr>
										<td><label class="col-form-label" for="userName"><b>User Name: </b></label> </td>
										<td><input type="text" class="form-control-sm" id="userName"></td>
										<td><label class="col-form-label" for="emailAddress"><b>Email Address: </b></label> </td>
										<td><input type="email" class="form-control-sm" id="emailAddress"></td>
									</tr>
									<tr>
										<td colspan="4">
											<label class="col-form-label" for="password"> <b>Password:</b> </label><br /><input type="password" class="form-control-sm" id="password" style="width:100%">
											<label class="col-form-label" for="reEnterPassword"> <b>Re-enter Password:</b> </label><br /><input type="password" class="form-control-sm" id="reEnterPassword" style="width:100%">
										</td>
									</tr>
									<tr>
										<td colspan="1"><b>Account Type:</b></td>
										<td colspan="3">
											<div style="margin-left:25%">
												<label class="form-check-label" style="margin-left:10%">
													<input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
													Professor
												</label>


												<label class="form-check-label" style="margin-left:10%">
													<input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios2" value="option2">
													Learning Assistant
												</label>


												<label class="form-check-label" style="margin-left:10%">
													<input type="radio" class="form-check-input" name="optionsRadios" id="optionsRadios3" value="option3">
													Admin
												</label>
											</div>
										</td>
									</tr>
								</table>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary">Add</button>
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								</div>
							</form>
						</div>

					</div>
				</div>
			</div>


		</div>

		<!--Footer-->
		<footer class="footer bg-light">
			<div class="container" id="footer-container">
				<span class="text-muted" style="font-size:20px;">@ 2018 Copyright: Tinkers</span>
			</div>
		</footer>

</body>
</html>