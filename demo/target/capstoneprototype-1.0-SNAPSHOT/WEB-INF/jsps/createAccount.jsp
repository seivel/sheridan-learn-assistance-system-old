<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Question Editor</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="<c:url value="/scripts/bootstrap.min.js" />"></script>
	<script src="<c:url value="/scripts/script.js" />"></script>
	<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
	<link rel="stylesheet" href="<c:url value="/css/style.css"/> "/>
	<style>
		footer {
			position: fixed;
			left: 0;
			bottom: 0;
			width: 100%;
			text-align: center;
			height: 6%;
		}
		#footer-container{
			margin-top:1%;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<a class="navbar-brand" href="<c:url value="/"/>" style="margin-left:5%; font-size:20px!important;"><img
				src='<c:url value="/images/sheridanLogo.png"/>' alt="sheridan logo" width="30px" height="40.797px"
				style="margin-right:20px;"/>Sheridan Learning Assistance System</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
		        aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
	</nav>

	<div class="container">
		<div class="jumbotron">
			<div id="error"></div>

			<c:url value="/register" var="url"/>
			<form name="form" method="post" action="${url}" onsubmit="return verify()">

				<div class="form-group">
					<label>Username</label>
					<div>
						<input name="username" type="text" class="form-control"/></div>
				</div>
				<div class="form-group">
					<label>Password</label>
					<div>
						<input name="password" type="password" class="form-control"/></div>
				</div>
				<div class="form-group">
					<label>Re-enter Password</label>
					<div>
						<input name="verifyPassword" type="password" class="form-control"/></div>
				</div>

				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				<input type="submit" value="Create Account!" class="btn btn-primary" onclick="verify()"/>
			</form>

		</div>
	</div>

	<footer class="footer bg-light">
		<div class="container" id="footer-container">
			<span class="text-muted" style="font-size:20px;">@ 2018 Copyright: Tinkers</span>
		</div>
	</footer>
</body>
</html>