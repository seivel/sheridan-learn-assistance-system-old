<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="<c:url value="/scripts/bootstrap.min.js" />"></script>
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
<style>
      footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        text-align: center;
        height: 6%;
      }
      #footer-container{
        margin-top:1%;
      }
      .borderless td, .borderless th {
	    border: none;
	  }
	  
	  #body{
	  	width:90%;
	  	margin:auto;
	  	margin-top:1%;
	  }
	  
	  p{
	  	font-size:18px !important;
	  }
	  
	  a{
	 	font-size:20px !important;
	  }
    </style>
<title>Learning System Home</title>
</head>
<body>
	 <div class="container-fluid" style="padding:0">
      <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="<c:url value="/"/>" style="margin-left:5%"><img src='<c:url value="/images/sheridanLogo.png"/>' alt="sheridan logo" width="30px" height="40.797px" style="margin-right:20px;"/>Sheridan Learning Assistance System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
          <c:url value="/secure" var="url"/>
			<c:url value="/studentLoginForm" var="studentUrl"/>
          <form class="form-inline my-2 my-lg-0" style="margin-left: 70%;">
            <a href="${studentUrl}" class="btn btn-outline-secondary" style="margin-right: 25px;">Student Login</a>
            <a href="${url}" class="btn btn-outline-secondary">User/Admin Login</a>
          </form>
        </div>
      </nav>
    </div>

    <!--body-->
    <div id="body">
      <table class="table borderless">
      	<tr>
      		<td><h1><b>Welcome!</b></h1></td>
      		<td></td>
      	</tr>
      	<tr>
      		<td><p>Lorem ipsum dolor sit amet, sea modus putent cu, ei mei causae vituperatoribus, usu te percipit quaestio. Id facete minimum his, sea ut purto omittam efficiendi. Vim reque molestiae no. Nostro nostrum consulatu eu eos, mea ad odio utamur ancillae. Eirmod dolorem repudiare te mei, est ut noluisse senserit. Usu definitiones necessitatibus ad, nec melius minimum menandri ut, solet complectitur ne mei. Vix tale quas tibique no, et natum ludus quaeque pro.

Vis ex singulis delicatissimi, denique albucius mentitum est in. Vis nostro nominavi consectetuer eu, cu quod aperiri sea, ea dicat accusamus vel. Eam id mazim tantas. Homero quidam expetendis has ne. Graecis singulis consequuntur eum et. Id utroque antiopam tractatos sea. Est purto altera intellegam ex.</p>
</td>
      		<td style="text-align:center; padding-top:0;"><img src='<c:url value="/images/image-icon.png"/>' alt="imageicon" width="250px"/></td>
      	</tr>
      	
      	<tr>
      		<td></td>
      		<td><h1><b>Introduction</b></h1></td>
      	</tr>
      	<tr>
      		<td style="text-align:center; padding-top:0;"><img src='<c:url value="/images/image-icon.png"/>' alt="imageicon" width="250px"/></td>
      		<td><p>Lorem ipsum dolor sit amet, sea modus putent cu, ei mei causae vituperatoribus, usu te percipit quaestio. Id facete minimum his, sea ut purto omittam efficiendi. Vim reque molestiae no. Nostro nostrum consulatu eu eos, mea ad odio utamur ancillae. Eirmod dolorem repudiare te mei, est ut noluisse senserit. Usu definitiones necessitatibus ad, nec melius minimum menandri ut, solet complectitur ne mei. Vix tale quas tibique no, et natum ludus quaeque pro.

Vis ex singulis delicatissimi, denique albucius mentitum est in. Vis nostro nominavi consectetuer eu, cu quod aperiri sea, ea dicat accusamus vel. Eam id mazim tantas. Homero quidam expetendis has ne. Graecis singulis consequuntur eum et. Id utroque antiopam tractatos sea. Est purto altera intellegam ex.</p>
      		</td>
      	</tr>
      	<tr>
      		<td><h1><b>Instruction</b></h1><td>
      	</tr>
      	<tr>
      		<td><p>Lorem ipsum dolor sit amet, sea modus putent cu, ei mei causae vituperatoribus, usu te percipit quaestio. Id facete minimum his, sea ut purto omittam efficiendi. Vim reque molestiae no. Nostro nostrum consulatu eu eos, mea ad odio utamur ancillae. Eirmod dolorem repudiare te mei, est ut noluisse senserit. Usu definitiones necessitatibus ad, nec melius minimum menandri ut, solet complectitur ne mei. Vix tale quas tibique no, et natum ludus quaeque pro.

Vis ex singulis delicatissimi, denique albucius mentitum est in. Vis nostro nominavi consectetuer eu, cu quod aperiri sea, ea dicat accusamus vel. Eam id mazim tantas. Homero quidam expetendis has ne. Graecis singulis consequuntur eum et. Id utroque antiopam tractatos sea. Est purto altera intellegam ex.</p>
      		</td>
      		<td style="text-align:center; padding-top:0;"><img src='<c:url value="/images/image-icon.png"/>' alt="imageicon" width="250px"/></td>
      	</tr>
      	
      </table>

    </div>

    <!--Footer-->
    <footer class="footer bg-light">
      <div class="container" id="footer-container">
        <span class="text-muted" style="font-size:20px;">@ 2018 Copyright: Tinkers</span>
      </div>
    </footer>

	
</body>
</html>