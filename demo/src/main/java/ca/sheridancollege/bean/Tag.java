package ca.sheridancollege.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tag")
@NamedQueries({
        @NamedQuery(name = "Tag.getAll", query = "from Tag"),
        @NamedQuery(name = "Tag.getByCourse", query = "from Tag t join t.courseSet c " +
                "where c.id = :courseId")
})
public class Tag implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "tag_id", unique = true, nullable = false)
    private int id;

    @Column(name = "tag")
    private String tag;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "tagSet")
    private Set<Question> questionSet = new HashSet<>();

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "tagSet")
    private Set<Course> courseSet = new HashSet<>();

    public Tag(String tag) {
        this.tag = tag;
    }
}
