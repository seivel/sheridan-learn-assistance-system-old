package ca.sheridancollege.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "program")
@NamedQueries({
        @NamedQuery(name = "Program.getAll", query = "from Program")
})
public class Program implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "program_id", unique = true, nullable = false)
    private int id;

    @Column(name = "program")
    private String program;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "program")
    private Set<Course> courseSet = new HashSet<>();

    public Program(String program) {
        this.program = program;
    }
}
