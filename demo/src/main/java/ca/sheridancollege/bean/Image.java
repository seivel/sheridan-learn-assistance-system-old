package ca.sheridancollege.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "image")
public class Image implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "image_id", unique = true, nullable = false)
    private int id;

    @Column(name = "file_name", unique = true, nullable = false)
    private String fileName;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "imageSet")
    private Set<Question> questionSet = new HashSet<>();

    public Image(String fileName) {
        this.fileName = fileName;
    }
}
