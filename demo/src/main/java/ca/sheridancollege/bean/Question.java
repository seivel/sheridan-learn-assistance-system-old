package ca.sheridancollege.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "question")
@NamedQueries({
        @NamedQuery(name = "Question.getAll", query = "from Question " +
                "where available = true"),
        @NamedQuery(name = "Question.getById", query = "from Question " +
                "where available = true and id = :id")
})
public class Question implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "question_id", unique = true, nullable = false)
    private int id;

    @Column(name = "description")
    private String description;

    @Column(name = "difficulty")
    private int difficulty;

    @JsonIgnore
    @Column(name = "available")
    private boolean available;

    @JsonIgnore
    @Column(name = "num_of_complaint")
    private int numOfComplaint;

    @Column(name = "created_at")
    private LocalDateTime createAt;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "updated_at")
    private String updateAt;


    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "question_tag",
            joinColumns = @JoinColumn(name = "question_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tagSet = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "question_course",
            joinColumns = @JoinColumn(name = "question_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id")
    )
    private Set<Course> courseSet = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "question_image",
            joinColumns = @JoinColumn(name = "question_id"),
            inverseJoinColumns = @JoinColumn(name = "image_id")
    )
    private Set<Image> imageSet = new HashSet<>();

    public Question(String description, int difficulty) {
        this.description = description;
        this.difficulty = difficulty;
    }
}
