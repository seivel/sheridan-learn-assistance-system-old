package ca.sheridancollege.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "course")
@NamedQueries({
        @NamedQuery(name = "Course.getAll", query = "from Course"),
        @NamedQuery(name = "Course.getById", query = "from Course where id = :id"),
        @NamedQuery(name = "Course.getByProgram", query = "from Course c join c.program p " +
                "where p.id = :programId")
})
public class Course implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "course_id", unique = true, nullable = false)
    private int id;

    @Column(name = "course")
    private String course;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "courseSet")
    private Set<Question> questionSet = new HashSet<>();

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "course_tag",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tagSet = new HashSet<>();

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "program_id")
    private Program program;

    public Course(String course) {
        this.course = course;
    }
}
