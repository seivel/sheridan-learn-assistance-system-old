package ca.sheridancollege.controller;

import ca.sheridancollege.bean.*;
import ca.sheridancollege.dao.DAO;
import ca.sheridancollege.service.MyUserDetailsService;
import ca.sheridancollege.utility.QuestionBuilder;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class HomeController {

    private DAO dao = new DAO();

    public HomeController() {

//        testLoad();
    }

    /**
     * Redirect to home page
     *
     * @param model model
     * @return name of home page
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {

        return "home";
    }

    @RequestMapping(value = "/secure", method = RequestMethod.GET)
    public String secure(Model model) {

        model.addAttribute("userList", dao.getUserList());

        return "adminHome";
    }



    /**
     * Redirect to login page
     *
     * @param model model
     * @return name of login page
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {

        return "loginForm";
    }

    @RequestMapping(value = "/forgetPassword", method = RequestMethod.GET)
    public String forgetPassword(Model model) {

        return "forgetPassword";
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
    public String resetPassword(Model model) {

        return "resetPassword";
    }


    @RequestMapping(value = "/studentLoginForm", method = RequestMethod.GET)
    public String studentLoginForm(Model model) {

        return "studentLoginForm";
    }


    @RequestMapping(value = "/studentExamCram", method = RequestMethod.GET)
    public String studentExamCram(Model model) {

        return "studentExamCram";
    }

    @RequestMapping(value = "/examCram", method = RequestMethod.GET)
    public String examCram(Model model) {

        return "examCram";
    }

    @RequestMapping(value = "/questionsManagement", method = RequestMethod.GET)
    public String questionsManagement(Model model) {

        List<Course> courseList = dao.getCourseList();
        model.addAttribute("courseList",courseList);

        List<Tag> tagList = dao.getTagList();
        model.addAttribute("tagList",tagList);

        Question question = new Question();
        model.addAttribute("question",question);

        List<Question> questionList = dao.getQuestionList();
        model.addAttribute("questionList",questionList);


        return "questionsManagement";
    }

    @RequestMapping(value = "/tagsManagement", method = RequestMethod.GET)
    public String tagsManagement(Model model) {

        return "tagsManagement";
    }

    @RequestMapping(value = "/reportDashboard", method = RequestMethod.GET)
    public String reportDashboard(Model model) {

        return "reportDashboard";
    }


    @RequestMapping(value = "/createAccount", method = RequestMethod.GET)
    public String createAccount(Model model){
        return "createAccount";
    }

 /*   @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(Model model, @RequestParam String username, @RequestParam String password) {

        String encryptedPassword = new BCryptPasswordEncoder().encode(password);
        User user = new User(username, encryptedPassword, true);

        UserRole userRole = new UserRole(user, "ROLE_USER");
        user.getUserRole().add(userRole);

        DAO dao = new DAO();
        dao.saveOrUpdate(user);

        UserDetails userDetails = new MyUserDetailsService().loadUserByUsername(username);
        UsernamePasswordAuthenticationToken auth =
                new UsernamePasswordAuthenticationToken(userDetails, encryptedPassword, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);

//        model.addAttribute("accountCreated", true);
        return "home";
    }*/

    /**
     * Redirect to add question page
     *
     * @param model model
     * @return name of add question page
     */
    @RequestMapping(value = "/addQuestion", method = RequestMethod.GET)
    public String addQuestion(Model model) {

        model.addAttribute("question", new Question());
        model.addAttribute("program", dao.getProgramList());
        model.addAttribute("course", dao.getCourseByProgram(1));
        model.addAttribute("tags", dao.getTagByCourse(3));

        return "questionEditor";
    }

    /**
     * Used to save question
     *
     * @param model    model
     * @param question question pojo
     *                 ...
     * @return ...
     */
    @RequestMapping(value = "/saveQuestion", method = RequestMethod.POST)
    public String saveQuestion(Model model, @ModelAttribute Question question,
                               @RequestParam(required = false) Course[] course,
                               @RequestParam(required = false) Tag[] tag,
                               @RequestParam(required = false) int difficulty) {

//        question.setAvailable(true);
//        question.setDifficulty(difficulty);
//        dao.saveOrUpdate(QuestionBuilder.buildQuestion(question, course, new Image[1], tag));
        question.setAvailable(true);
        dao.saveOrUpdate(question);

        model.addAttribute("questionList", dao.getQuestionList());

        List<Course> courseList = dao.getCourseList();
        model.addAttribute("courseList",courseList);

        List<Tag> tagList = dao.getTagList();
        model.addAttribute("tagList",tagList);

        Question newQuestion = new Question();
        model.addAttribute("question",newQuestion);

        return "questionsManagement";
    }

    @RequestMapping(value = "/reports", method=RequestMethod.GET)
    public String reports(Model model){
        return "reports";
    }

    /**
     * Redirect to view questions page
     *
     * @param model model
     * @return name of view question page
     */
    @RequestMapping(value = "/viewQuestion", method = RequestMethod.GET)
    public String viewQuestion(Model model) {

        return "";
    }

    /**
     * Return a json that contains information of questions
     *
     * @return json
     */
    @RequestMapping(value = "/retrieveQuestion", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Map<String, Object> retrieveQuestion() {

        Map<String, Object> model = new HashMap<>(1);
        model.put("QuestionList", dao.getQuestionList());
        return model;
    }

    /**
     * Redirect to edit question page (should be same as add new question page)
     *
     * @return name of edit question page
     */
    @RequestMapping(value = "/editQuestion/{questionId}", method = RequestMethod.GET)
    public String editQuestion(Model model, @PathVariable int questionId) {

        model.addAttribute("question", dao.getQuestionById(questionId));
        model.addAttribute("program", dao.getProgramList());
        model.addAttribute("course", dao.getCourseByProgram(1));
        model.addAttribute("tags", dao.getTagByCourse(3));
        return "questionEditor";
    }

    @RequestMapping(value = "/retrieveQuestionById/{questionId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Map<String, Object> retrieveQuestionById(@PathVariable int questionId) {

        Map<String, Object> model = new HashMap<>(1);
        model.put("QuestionList", dao.getQuestionById(questionId));
        return model;
    }

    /**
     * Used to disable a question
     *
     * @return ...
     */
    @RequestMapping(value = "/disableQuestion/{questionId}", method = RequestMethod.GET)
    public String disableQuestion(Model model, @PathVariable int questionId) {

        dao.disableQuestion(questionId);

        model.addAttribute("questionList", dao.getQuestionList());
        return "adminHome";
    }

    /**
     * Return a json that contains all programs
     *
     * @return json
     */
    @RequestMapping(value = "/retrieveProgram", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Map<String, Object> retrieveProgram() {

        Map<String, Object> model = new HashMap<>(1);
        model.put("ProgramList", dao.getProgramList());
        return model;
    }

    /**
     * Return a json that contains course information for a given program
     *
     * @param programId program id
     * @return json
     */
    @RequestMapping(value = "/retrieveCourse/{programId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Map<String, Object> retrieveCourse(@PathVariable int programId) {

        Map<String, Object> model = new HashMap<>(1);
        model.put("CourseList", dao.getCourseByProgram(programId));
        return model;
    }

    /**
     * Return a json that contains tag information for a given course
     *
     * @param courseId course id
     * @return json
     */
    @RequestMapping(value = "/retrieveTag/{courseId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Map<String, Object> retrieveTag(@PathVariable int courseId) {

        Map<String, Object> model = new HashMap<>(1);
        model.put("TagList", dao.getTagByCourse(courseId));
        return model;
    }

    private void testLoad() {

        Program program = new Program("Computing");

        Course course = new Course("Java 1");
        course.setProgram(program);

        dao.saveOrUpdate(program);

        Tag[] tags = new Tag[]{
                new Tag("tag1"),
                new Tag("tag2"),
                new Tag("tag3"),
                new Tag("tag4"),
                new Tag("tag5")
        };
        course.getTagSet().addAll(Arrays.asList(tags));

        Question[] questions = new Question[]{
                new Question("test question 1", 1),
                new Question("test question 2", 1),
                new Question("test question 3", 1)
        };

        Image[] images = new Image[]{
                new Image("testImage1"),
                new Image("testImage2")
        };

        questions[0].getImageSet().add(images[0]);
        questions[1].getImageSet().add(images[1]);

        questions[0].getTagSet().addAll(Arrays.asList(tags[1], tags[2], tags[4]));
        questions[1].getTagSet().addAll(Arrays.asList(tags[0], tags[2]));
        questions[2].getTagSet().addAll(Arrays.asList(tags[3]));

        Arrays.stream(questions).forEach(question -> question.getCourseSet().add(course));

        Arrays.stream(questions).forEach(dao::saveOrUpdate);
    }
}
