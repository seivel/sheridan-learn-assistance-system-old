package ca.sheridancollege.dao;

import java.util.List;

public interface BaseDAO<T> {

    void saveOrUpdate(T entity);

    List<T> getAll();

    T getById();
}
