package ca.sheridancollege.dao;

import ca.sheridancollege.bean.Question;

import java.util.List;

public class QuestionDAOImpl extends BaseDAOImpl<Question> implements BaseDAO<Question> {

    @Override
    public void saveOrUpdate(Question entity) {
        super.saveOrUpdate(entity);
    }

    @Override
    public List<Question> getAll() {
        return null;
    }

    @Override
    public Question getById() {
        return null;
    }
}
