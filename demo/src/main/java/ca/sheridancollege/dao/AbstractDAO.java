package ca.sheridancollege.dao;

import ca.sheridancollege.utility.HibernateUtil;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class AbstractDAO<T> {

    protected void saveOrUpdate(T entity) {
        Session session = HibernateUtil.SESSION_FACTORY
                .getSessionFactory().openSession();
        session.beginTransaction();

        session.saveOrUpdate(entity);

        session.getTransaction().commit();
        session.close();
    }

    protected List<T> getAll(String entityName) {
        return getByQuery(entityName + ".getAll");
    }

    protected T getById(String entityName, int id) {
        Session session = HibernateUtil.SESSION_FACTORY
                .getSessionFactory().openSession();
        session.beginTransaction();

        @SuppressWarnings("unchecked")
        T result = (T) session.createNamedQuery(entityName + ".getById")
                .setParameter("id", id)
                .getSingleResult();

        session.getTransaction().commit();
        session.close();

        return result;
    }

    protected List<T> getByQuery(String query) {
        Session session = HibernateUtil.SESSION_FACTORY
                .getSessionFactory().openSession();
        session.beginTransaction();

        @SuppressWarnings("unchecked")
        List<T> resultList = session.createNamedQuery(query).getResultList();

        session.getTransaction().commit();
        session.close();

        return resultList;
    }

    protected List<T> getByCriteria(Class<T> entity, String searchFor, String search) {
        Session session = HibernateUtil.SESSION_FACTORY
                .getSessionFactory().openSession();
        session.beginTransaction();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteria = criteriaBuilder.createQuery(entity);
        Root<T> root = criteria.from(entity);

        criteria.where();

        List<T> resultList = session.createQuery(criteria)
                .getResultList();

        session.getTransaction().commit();
        session.close();

        return resultList;
    }
}
