package ca.sheridancollege.dao;

import ca.sheridancollege.bean.*;
import ca.sheridancollege.utility.HibernateUtil;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;

public class DAO extends AbstractDAO {

    @Override
    public void saveOrUpdate(Object entity) {
        super.saveOrUpdate(entity);
    }

    @SuppressWarnings("unchecked")
    public List<Program> getProgramList() {
        return super.getAll("Program");
    }

    @SuppressWarnings("unchecked")
    public List<Course> getCourseList() {
        return super.getAll("Course");
    }

    public List<Tag> getTagList(){return super.getAll("Tag"); }

    public List<User> getUserList(){return super.getAll("User");}

    public Program getProgramById(int id) {
        return (Program) super.getById("Program", id);
    }

    public Course getCourseById(int id) {
        return (Course) super.getById("Course", id);
    }

    public List<Course> getCourseByProgram(int programId) {
        Session session = HibernateUtil.SESSION_FACTORY
                .getSessionFactory().openSession();
        session.beginTransaction();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Course> criteria = criteriaBuilder.createQuery(Course.class);
        Root<Course> root = criteria.from(Course.class);

        criteria.where(criteriaBuilder.equal(root.get("program"), programId));

        List<Course> courseList = session.createQuery(criteria)
                .getResultList();

        session.getTransaction().commit();
        session.close();

        return courseList;
    }

    public List<Tag> getTagByCourse(int courseId) {
        Session session = HibernateUtil.SESSION_FACTORY
                .getSessionFactory().openSession();
        session.beginTransaction();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Tag> criteria = criteriaBuilder.createQuery(Tag.class);
        Root<Tag> root = criteria.from(Tag.class);

        criteria.where(criteriaBuilder.isMember(courseId, root.get("courseSet")));

        List<Tag> tagList = session.createQuery(criteria)
                .getResultList();

        session.getTransaction().commit();
        session.close();

        return tagList;
    }


    public List<Question> getQuestionList() {
        return super.getAll("Question");
    }

    public Question getQuestionById(int id) {
        return (Question) super.getById("Question", id);
    }

    public void disableQuestion(int id) {
        Question question = getQuestionById(id);
        question.setAvailable(false);
        saveOrUpdate(question);
    }

    public User findByUserName(String username) {
        Session session = HibernateUtil.SESSION_FACTORY
                .getSessionFactory().openSession();
        session.beginTransaction();

        User user = (User) session.createNamedQuery("User.getByUsername")
                .setParameter("username", username)
                .getSingleResult();

        session.getTransaction().commit();
        session.close();

        return user;
    }
}
