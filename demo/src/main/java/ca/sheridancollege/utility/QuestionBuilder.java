package ca.sheridancollege.utility;

import ca.sheridancollege.bean.Course;
import ca.sheridancollege.bean.Image;
import ca.sheridancollege.bean.Question;
import ca.sheridancollege.bean.Tag;

import java.util.Arrays;

public class QuestionBuilder {

    public static Question buildQuestion(Question question, Course[] courses, Image[] images, Tag[] tags){

        question.getCourseSet().addAll(Arrays.asList(courses));
        question.getImageSet().addAll(Arrays.asList(images));
        question.getTagSet().addAll(Arrays.asList(tags));

        return question;
    }
}
