package ca.sheridancollege.utility;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class PredicateUtil {

    public static Predicate getLikePredicate(String searchFor, String search,
                                             CriteriaBuilder criteriaBuilder, Root<?> root) {
        return criteriaBuilder.like(root.get(searchFor), "%" + search + "%");
    }

    public static Predicate getEqualPredicate(String searchFor, String search,
                                              CriteriaBuilder criteriaBuilder, Root<?> root) {
        return criteriaBuilder.equal(root.get(searchFor), search);
    }

}
