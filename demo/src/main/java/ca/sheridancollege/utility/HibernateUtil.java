package ca.sheridancollege.utility;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public enum HibernateUtil {
    SESSION_FACTORY(new Configuration()
            .configure("hibernate.cfg.xml")
            .buildSessionFactory());

    private SessionFactory sessionFactory;

    HibernateUtil(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
